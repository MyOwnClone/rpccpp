#include <functional>
#include <iostream>

// prepsano z http://babel.blog.root.cz/2014/04/02/promenlive-sablony-a-rpc-v-c/

template<typename...Args> struct List {};

template<> struct List<> {
	List() {}
};

template<typename H, typename...T> struct List<H, T...> {
	H head;
	List<T...> tail;
	List() 
	{

	}

	List(const H& h, const T&... t) : head(h), tail(t...) 
	{

	}
	
	void iterate(const std::function<void(const H&)>& callback) const 
	{ 
		callback(head); 
		tail.iterate(callback); 
	}
};

template<typename H> struct List<H> {
	H head;
	List<> tail;
	List() 
	{
	
	}
	List(const H& h) : head(h) 
	{

	}

	void iterate(const std::function<void(const H&)>& callback) const 
	{ 
		callback(head); 
	}
};

template<typename...Args> List<Args...> createList(const Args&...args) { 
	return List<Args...>(args...); 
}

template<typename H, typename...T> auto createList(const H& h, const List<T...>& t) -> decltype(List<H, T...>)
{ 
	List<H, T...> list; 
	list.head = h; 
	list.tail = t; 
	return list; 
}

template<int N> struct getel 
{
	template<typename H, typename...T> static H value(const List<H, T...>& list) 
	{ 
		return getel<N - 1>::value(list.tail);
	}
};

template<> struct getel<0> {
	template<typename H, typename...T> static H value(const List<H, T...>& list) 
	{
		return list.head; 
	}
};

template<typename Ret, typename...Args> class Caller 
{
private:
	Ret(*func)(Args...);
	List<Args...> list;
public:
	Caller(decltype(func) f, const decltype(list)& l) : func(f), list(l) 
	{

	}

	template<typename...A> typename std::enable_if<sizeof...(A) != sizeof...(Args), Ret>::type 
		invoke(const A&...a) { return invoke(a..., getel<sizeof...(A)>::value(list)); }
	Ret invoke(const Args&...args) { return (*func)(args...); }
};

template<typename Ret, typename...Args> Caller<Ret, Args...> createCaller(Ret(*func)(Args...), const List<Args...>& list) 
{ 
	return Caller<Ret, Args...>(func, list); 
}

int sum(int x, int y) 
{ 
	return x + y;
}

int main(int argc, char** argv)
{
	const auto& list = createList(2, 3);
	std::cout << createCaller(&sum, list).invoke() << std::endl;
}